import React from "react";
import { shallow } from "enzyme";
import Header from "./Header";
import renderer from "react-test-renderer";
import { findTestAttr } from "../../../utils";

const setUp = (props = {}) => {
  const component = shallow(<Header {...props} />);
  return component;
};

describe("Header component", () => {
  let component;
  beforeEach(() => {
    component = setUp();
  });

  it("snapshot header component", () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
  it("should render header without error", () => {
    const wrapper = findTestAttr(component, "header");
    expect(wrapper.length).toBe(1);
  });
  it("should render youtube icon", () => {
    const icon = findTestAttr(component, "youtube-icon");
    expect(icon.length).toBe(1);
  });
  it("should render youtube title", () => {
    const title = findTestAttr(component, "youtube-title");
    expect(title.length).toBe(1);
  });
});
