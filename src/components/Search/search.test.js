import React from "react";
import { shallow } from "enzyme";
import Search from "./Search";
import renderer from "react-test-renderer";
import { testStore, findTestAttr } from "../../../utils";

const setUp = (initialState = {}) => {
  const store = testStore(initialState);
  const wrapper = shallow(<Search store={store} />)
    .childAt(0)
    .dive();
  return wrapper;
};

describe("Search component", () => {
  let component;

  beforeEach(() => {
    component = setUp({});
  });
  it("snapshot search component", () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
  it("should render form", () => {
    const form = findTestAttr(component, "form");
    expect(form.length).toBe(1);
  });
  it("should render input", () => {
    const input = findTestAttr(component, "input");
    expect(input.length).toBe(1);
  });
});
