import React from "react";
import { shallow } from "enzyme";
import HomePage from "./HomePage";
import renderer from "react-test-renderer";
import { Provider } from "react-redux";
import { findTestAttr, testStore } from "../../../utils";
import { BrowserRouter } from "react-router-dom";
const setUp = (initialState = {}, props = {}) => {
  const store = testStore(initialState);
  const wrapper = shallow(<HomePage store={store} {...props} />)
    .childAt(0)
    .dive();
  return wrapper;
};

describe("homepage component", () => {
  let component;
  const props = {
    location: {
      state: "",
    },
  };
  beforeEach(() => {
    component = setUp({}, props);
  });
  it("snapshot homepage component", () => {
    const store = testStore({});
    const tree = renderer.create(
      <Provider store={store}>
        <BrowserRouter>
          <HomePage {...props} />
        </BrowserRouter>
      </Provider>
    );
    expect(tree).toMatchSnapshot();
  });

  it("should render homepage header", () => {
    const header = findTestAttr(component, "homepage-header");
    expect(header.length).toBe(1);
  });
});
