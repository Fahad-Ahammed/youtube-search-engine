import React from "react";
import "./Video.css";
export default function Video({ videoDetails }) {
  return (
    <>
      {videoDetails.map((video, index) => {
        return (
          <div
            key={index}
            className="video-conatainer"
            data-test="video-container"
          >
            <img
              className="image"
              src={video.snippet.thumbnails.medium.url}
              alt=""
              data-test="image"
            />
            <div className="video-details" data-test="video-details">
              <p className="video-title">{video.snippet.title}</p>
              <p className="channel-title">{video.snippet.channelTitle}</p>
            </div>
          </div>
        );
      })}
    </>
  );
}
