import React from "react";
import { shallow } from "enzyme";
import LoginPage from "./LoginPage";
import renderer from "react-test-renderer";
import { findTestAttr, testStore } from "../../../utils";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";

const setUp = (initialState = {}, props = {}) => {
  const store = testStore(initialState);
  const wrapper = shallow(<LoginPage store={store} {...props} />).dive();
  return wrapper;
};

describe("loginPage component", () => {
  let component;
  beforeEach(() => {
    component = setUp();
  });
  it("snapshot homepage component", () => {
    const store = testStore({});
    const tree = renderer.create(
      <Provider store={store}>
        <BrowserRouter>
          <LoginPage />
        </BrowserRouter>
      </Provider>
    );
    expect(tree).toMatchSnapshot();
  });
  it("should render loginpage", () => {
    const wrapper = findTestAttr(component, "login-form");
    expect(wrapper.length).toBe(1);
  });
});
