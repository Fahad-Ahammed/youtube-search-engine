const response = {
  kind: "youtube#searchListResponse",
  nextPageToken: "CAgQAA",
  prevPageToken: "CAQQAQ",
  items: [
    {
      
      snippet: {
        publishedAt: "2019-11-17T07:24:00Z",
        channelId: "UCvoJoI339M14dqYTPQZ56Rg",
        title: "test title",
        description: "test description",
        channelTitle: "test channel title",
        liveBroadcastContent: "none",
        publishTime: "2019-11-17T07:24:00Z",
        thumbnails: {
          medium: {
            url: "test url",
          },
        },
      },
    },
    {
      
      snippet: {
        publishedAt: "2019-11-17T07:24:00Z",
        channelId: "UCvoJoI339M14dqYTPQZ56Rg",
        title: "test title",
        description: "test description",
        channelTitle: "test channel title",
        liveBroadcastContent: "none",
        publishTime: "2019-11-17T07:24:00Z",
        thumbnails: {
          medium: {
            url: "test url",
          },
        },
      },
    },
  ],
};

export default response;
