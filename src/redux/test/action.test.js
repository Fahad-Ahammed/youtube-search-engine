import { searchActionTypes } from "../search/search-types";
import dummyResponse from "./dummy-response";
import {
  NewSearchItem,
  getVideos,
  getTrending,
  infiniteScroll,
  login,
} from "../search/search-action";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import moxios from "moxios";
const mockStore = configureMockStore([thunk]);

describe("should test all action", () => {
  it("should test newSearchItem action", () => {
    const search = "apple";
    const expectedAction = {
      type: searchActionTypes.NEW_SEARCH_TITLE,
      payload: search,
    };
    expect(NewSearchItem(search)).toEqual(expectedAction);
  });

  it("should test login action", () => {
    const expectedAction = {
      type: searchActionTypes.LOGIN,
    };
    expect(login()).toEqual(expectedAction);
  });

  describe("should test asynchronous actions", () => {
    beforeEach(() => {
      moxios.install();
      moxios.wait(() => {
        const request = moxios.requests.mostRecent();
        request.respondWith({
          response: dummyResponse,
        });
      });
    });

    afterEach(() => {
      moxios.uninstall();
    });

    it("should test getVideos action", () => {
      const store = mockStore();
      return store.dispatch(getVideos("apple", 2)).then(() => {
        const action = store.getActions();
        const expectedAction = {
          type: searchActionTypes.SEARCH_ITEM_RESULT,
          payload: dummyResponse,
        };
        expect(action[0]).toEqual(expectedAction);
      });
    });

    it("should test getTrending action", () => {
      const store = mockStore();
      return store.dispatch(getTrending(5)).then(() => {
        const action = store.getActions();
        const expectedAction = {
          type: searchActionTypes.TRENDING,
          payload: dummyResponse,
        };
        expect(action[0]).toEqual(expectedAction);
      });
    });

    it("should test infiniteScroll action", () => {
      const store = mockStore();
      return store
        .dispatch(infiniteScroll("apple", 2, "testToken"))
        .then(() => {
          const action = store.getActions();
          const expectedAction = {
            type: searchActionTypes.INFINITE_SCROLL,
            payload: dummyResponse,
          };
          expect(action[0]).toEqual(expectedAction);
        });
    });
  });
});
